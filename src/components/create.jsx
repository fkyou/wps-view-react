import React from "react"
import {getCreateFilePath} from '../api/index'
import {message,Spin} from "antd";

export default class CreateFilePage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            src:''
        }
    }

    componentDidMount() {
        this.getCreateFilePath(sessionStorage.createFileType);
    }

    getCreateFilePath = (type)=>{
        this.openLoading();
        getCreateFilePath({template:type}).then(res=>{
            if (res.data.data){
                this.setState({
                    src: res.data.data
                })
            }else {
                message.error("请求异常");
            }
            this.cleanLoading();
        }).catch(()=>{
            this.cleanLoading();
            message.error("请求异常");
        });
    };

    openLoading = ()=>{
        this.setState({
            loading: true
        });
    };

    cleanLoading = ()=>{
        this.setState({
            loading: false
        });
    };

    render() {
        return <div id={'create'}>
            <Spin spinning={this.state.loading} style={{marginTop:'15%'}}/>
            <iframe title={'createDocContent'} ref="docContent" className="frame-content" src={this.state.src}/>
        </div>
    }

}
